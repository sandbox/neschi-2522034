<?php

/**
 * @file
 * Administration page callbakcs for the Webform Selectable Filter module.
 */

/**
 * Form builder.
 */
 
/**
 * Menu callback for admin/config/content/webform_selectable_filters.
 */
function webform_selectable_filters_admin_settings() {
  $form['contact_information'] = array(
    '#markup' => variable_get('webform_selectable_filters_admin_markup',
    t('Select the webform and the view that would filter the submissions.')),
  );
  $form['webform_selectable_filters_webform'] = array(
    '#type' => 'radios',
    '#title' => t('Webform'),
    '#description' => t('Select the Webform in which apply this module.'),
    '#options' => webform_selectable_filters_get_webforms(),
    '#default_value' => variable_get('webform_selectable_filters_webform', NULL),
    '#size' => 30,
  );
  $form['webform_selectable_filters_view'] = array(
    '#type' => 'radios',
    '#title' => t('View'),
    '#description' => t('Select the View in which apply this module.'),
    '#options' => webform_selectable_filters_get_views(),
    '#default_value' => variable_get('webform_selectable_filters_view', NULL),
    '#size' => 30,
  );
  return system_settings_form($form);
}

// Get all select components for the webform.
function webform_selectable_filters_get_webforms() {
  $webforms = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', 'webform', '=')
    ->execute()
    ->fetchAllAssoc('nid');

  foreach ($webforms as $webform) {
    $webforms_processed[$webform->nid] = t($webform->title);
  }
  return $webforms_processed;
}

// Get all craeted views.
function webform_selectable_filters_get_views() {
  $views = db_select('views_view', 'v')
    ->fields('v', array('vid', 'human_name'))
    ->execute()
    ->fetchAllAssoc('vid');

  foreach ($views as $view) {
    $view_processed[$view->vid] = t($view->human_name);
  }
  return $view_processed;
}
