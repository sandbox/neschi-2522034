<?php

/*
 * Implements hook_views_query_alter().
 * 
 * Allows to exposed filters to accept multiple values.
 * Text fields and select boxes only.
 */
function webform_selectable_filters_views_query_alter(&$view, &$query) {
  if (!path_is_admin(current_path())) {
    $vid = variable_get('webform_selectable_filters_view');

    if ($view->vid == $vid) {
      foreach ($query->where as $k => $group) {
        foreach ($query->where[$k]['conditions'] as $key => &$condition) {
          // If exposed filter is a select box.
          if (is_array($condition['value']) && count($condition['value']) > 1) {
            foreach ($condition['value'] as $value) {
              $query->add_where($k, $condition['field'], array($value => $value), 'LIKE');
            }
          unset($query->where[$k]['conditions'][$key]);

          // If exposed filter is a text field.
          } elseif (is_string($condition['value']) && strpos($condition['value'], '+') != FALSE) {
            $string = explode('+', $condition['value']);
            foreach ($string as $value) {
              $query->add_where($k, $condition['field'], $value, 'LIKE');
            }
          unset($query->where[$k]['conditions'][$key]);
          }
        }
      }
    }
  }
}